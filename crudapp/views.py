from django.shortcuts import render
from .models import Grade,Subject,Section
from .admin import GradeAdmin
from .forms import adminForm
def  home(request):
    if request.method== "POST":
        form=adminForm(request.POST)
        if form.is_valid():
            form.save()

    else:
        form=adminForm()
    return render(request,'insert.html',{"form":form})



