from django import forms
from .models import Grade,Subject,Section
from .admin import GradeAdmin

class adminForm(forms.ModelForm):
    class Meta:
        model=Grade
        fields="__all__"