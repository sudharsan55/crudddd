from django.db import models

# Create your models here.
class Grade(models.Model):
    grade=models.CharField(primary_key=True,max_length=50)

    def __str__(self):
        return self.grade


class Section(models.Model):
    grades=models.ForeignKey(Grade,on_delete=models.CASCADE,related_name="grades")
    sec=models.CharField(max_length=50,default='Enter')

    def __str__(self):
        return self.sec

class Subject(models.Model):
    section=models.ForeignKey('section',on_delete=models.CASCADE,related_name="section")
    subject=models.CharField(max_length=50,default='Enter')

    def __str__(self):
        return self.subject

