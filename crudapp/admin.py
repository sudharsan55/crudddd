from django.contrib import admin

# Register your models here.
from .models import Grade,Section,Subject

class GradeAdmin(admin.ModelAdmin):
    list_display = ['grade']

admin.site.register(Grade,GradeAdmin)
class SectionAdmin(admin.ModelAdmin):
    list_display = ['grades','sec']
admin.site.register(Section,SectionAdmin)
class SubjectAdmin(admin.ModelAdmin):
    list_display = ['section','subject']
admin.site.register(Subject,SubjectAdmin)
#
# class ChoiceAdmin(admin.ModelAdmin):
#     pass
# admin.site.register(Choice, ChoiceAdmin)